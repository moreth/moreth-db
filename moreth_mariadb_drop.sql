--
-- Supression des tables en respectant les foreign keys
--
drop view V_INFO_SEARCH;
drop table ENTITY_AFFAIR;
drop table AFFAIR_INFO;
drop table AFFAIR_LINK_INFO;
drop table ENTITY_INFO;
drop table ENTITY_LINK_INFO;
drop table INFO_TYPE;
drop table INFO_GROUP;
drop table AFFAIR_LINK;
drop table ENTITY_LINK;
drop table AFFAIR;
drop table ENTITY;
